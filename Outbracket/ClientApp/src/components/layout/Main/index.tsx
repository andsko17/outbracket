import React, {FC} from 'react';

const Main: FC = ({children}) => {
  return <div>{children}</div>;
};

export default Main;
